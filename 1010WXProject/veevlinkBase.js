/**
 * Created by Luby on 2019/1/25.
 */
const base = {
   IsTest :false,
  /*
  本地账号是否启用 true 的话记得
  修改 SessionId(sf的session，可以问创发部的人) ，
     AppId(公众号的appid)，
     OpenId(微信用户唯一id)
     反向代理的域名请到 vue.config.js  66行处 修改对应的请求地址
   */
   domain:"https://ap2.veevlink.com", // 测试环境地址还是正式环境地址,正式环境地址可以问创发
   AppId :"wx1db98bbf8c39fd97",
   SessionId: "00D5D0000008z8h!ARUAQEyFfCxNiiak1Gc_BMFGvUAe_5eY5KLFsGPE804_2hinFoOzFCf.AX70FqvRfsFEr33ijvPHn6YUQRxnKgel85l0MZgl",
   OpenId : "oYwRi1WJOiCng_-M43EFvIw4oxLQ",
   InstanceUrl : "http://localhost:8080", // 本地请求的接口地址
   target : "https://cs72.salesforce.com",
  // ap2.veevlink.com 正式   https://test.veevlink.com 测试
   error : function () {
     // 错误收集
     window.onerror = function (msg, url, line, col, error) {
       //没有URL不上报
       if (msg != "Script error." && !url) {
         return true;
       }
       setTimeout(function () {
         var data = {};
         //不一定所有浏览器都支持col参数
         col = col || (window.event && window.event.errorCharacter) || 0;

         data.url = url;
         data.line = line;
         data.col = col;
         if (!!error && !!error.stack) {
           //如果浏览器有堆栈信息
           //直接使用
           data.msg = error.stack.toString();
         } else if (!!arguments.callee) {
           //尝试通过callee拿堆栈信息
           var ext = [];
           var f = arguments.callee.caller, c = 3;
           //这里只拿三层堆栈信息
           while (f && (--c > 0)) {
             ext.push(f.toString());
             if (f === f.caller) {
               break;//如果有环
             }
             f = f.caller;
           }
           ext = ext.join(",");
           data.msg = error.stack.toString();
         }
         //把data上报到后台！
         console.log('错误信息',data);
       }, 0);
       return true;
     };

   }
};

export default
{
  IsTest : base.IsTest,
  AppId: base.AppId,
  SessionId: base.SessionId,
  OpenId : base.OpenId,
  InstanceUrl : base.InstanceUrl,
  error : base.error,
  domain : base.domain
}
