import Vue from 'vue'
import Router from 'vue-router'
const home = r => require.ensure([], () => r(require('../page/home/home.vue')), 'home');
const phoneNum = r => require.ensure([], () => r(require('../page/phoneNum/phoneNum.vue')), 'phoneNum');
const msgVerify = r => require.ensure([], () => r(require('../page/msgVerify/msgVerify.vue')), 'msgVerify');
const expenseDetail = r => require.ensure([], () => r(require('../page/expenseDetail/expenseDetail.vue')), 'expenseDetail');
const integralRecord = r => require.ensure([], () => r(require('../page/integralRecord/integralRecord.vue')), 'interalRecord');
const memberRights = r => require.ensure([], () => r(require('../page/memberRights/memberRights.vue')), 'memberRights');
const myCount1 = r => require.ensure([], () => r(require('../page/myCount1/myCount1.vue')), 'myCount1');
const myCount3 = r => require.ensure([], () => r(require('../page/myCount3/myCount3.vue')), 'myCount3');
const personInfo = r => require.ensure([], () => r(require('../page/personInfo/personInfo.vue')), 'personInfo');
const result = r => require.ensure([], () => r(require('../page/result/result.vue')), 'result');
const page = r => require.ensure([], () => r(require('../page/page/page.vue')), 'page');
const error = r => require.ensure([], () => r(require('../page/error/error.vue')), 'error');
Vue.use(Router);
// survey
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'  // 任何没有具体路径的访问，我都让它重定向到home主页，重定向在限制用户手动修改URL时误操作很管用
    },
    //加载页
      {
          path: '/page',
          name: 'page',
          meta: {
              title: '1010HOPE希望生活志',
              allowBack: false
          },
          component: page
      },
    //会员主页
    {
      path: '/home',
      name: 'home',
      meta: {
        title: '1010HOPE希望生活志',
        allowBack: false
      },
      component: home
    },
    //手机号码
    {
      path: '/phoneNum',
      name: 'phoneNum',
      meta: {
        title: '1010HOPE希望生活志',
        allowBack: false
      },
      component: phoneNum
    },
    //短信验证
    {
      path: '/msgVerify',
      name: 'msgVerify',
      meta: {
        title: '1010HOPE希望生活志',
        allowBack: false
      },
      component: msgVerify
    },
    //填写基本资料
    {
      path: '/personInfo',
      name: 'personInfo',
      meta: {
        title: '1010HOPE希望生活志',
        allowBack: false
      },
      component: personInfo
    },
    //绑定完成
    {
      path: '/result',
      name: 'result',
      meta: {
        title: '1010HOPE希望生活志',
        allowBack: false
      },
      component: result
    },
    //积分记录详情
    {
      path: '/expenseDetail',
      name: 'expenseDetail',
      meta: {
        title: '消费明细',
        allowBack: true
      },
      component: expenseDetail
    },
    //积分记录列表
    {
      path: '/integralRecord',
      name: 'integralRecord',
      meta: {
        title: '积分记录',
        allowBack: true
      },
      component: integralRecord
    },
    //会员权益
    {
      path: '/memberRights',
      name: 'memberRights',
      meta: {
        title: '会员权益',
        allowBack: true
      },
      component: memberRights
    },
    //个人信息
    {
      path: '/myCount1',
      name: 'myCount1',
      meta: {
        title: '个人信息',
        allowBack: true
      },
      component: myCount1
    },
    //个人信息修改
    {
      path: '/myCount3',
      name: 'myCount3',
      meta: {
        title: '个人信息修改',
        allowBack: true
      },
      component: myCount3
    },
      //错误页面
      {
          path: '/error',
          name: 'error',
          meta: {
              title: '网页错误',
              allowBack: true
          },
          component: error
      },


  ]
})
