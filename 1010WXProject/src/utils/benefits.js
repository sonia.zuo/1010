$(function(){
    const activeCard = 'card_list_item--active'
    const cardBtn = $('.card_list_item')
// 各卡收合事項
    cardBtn.on('click',function(){
        let _this = $(this)
        if (!_this.hasClass(activeCard)){
            cardBtn.removeClass(activeCard)
            cardBtn.find('ul.card_sort_terms.mb_ctn').css({
                display: 'none',
                height: '0'
            })
            _this.addClass(activeCard)
            _this.find('ul.card_sort_terms.mb_ctn').css({
                display: 'block',
                height: 58 + 'vh'
            })
            return
        }
        _this.find('ul.card_sort_terms.mb_ctn').css({
            display: 'none',
            height: 0 + 'vh'
        })
        _this.removeClass(activeCard)
    })
})
